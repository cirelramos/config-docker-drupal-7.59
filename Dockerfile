FROM drupal:7.59-apache
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN cp -a composer.phar /usr/local/bin/composer
RUN apt-get update -y
RUN apt-get install git-core -y
#RUN composer require drupal/console:~1.0 --prefer-dist --optimize-autoloader
# Get a global installation of Drush on the web / drupal container
#RUN php -r "readfile('http://files.drush.org/drush.phar');" > drush && chmod +x drush && mv drush /usr/bin/
#RUN composer global require drush/drush && \
#composer global update
RUN export PATH="$HOME/.composer/vendor/bin:$PATH"
#COPY "memory-limit-php.ini" "/usr/local/etc/php/conf.d/memory-limit-php.ini"
RUN touch /usr/local/etc/php/conf.d/uploads.ini \
			&& echo "upload_max_filesize = 10M;" >> /usr/local/etc/php/conf.d/uploads.ini
